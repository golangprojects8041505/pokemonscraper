package database

type PokemonModel struct {
	Name        string `gorm:"type:varchar(100);unique;not null"`
	Description string `gorm:"text;not null"`
	PokedexID   string `gorm:"type:varchar(100);not null"`
	Species     string `gorm:"type:varchar(100);not null"`
	Types       string `gorm:"type:varchar(100);not null"`
	Height      string `gorm:"type:varchar(100);not null"`
	Weight      string `gorm:"type:varchar(100);not null"`
	Evolutions  string `gorm:"type:varchar(100)"`
}

type TypeModel struct {
	Name       string `gorm:"type:varchar(100);unique;not null"`
	Strengths  string `gorm:"type:varchar(100);not null"`
	Weaknesses string `gorm:"type:varchar(100);not null"`
}

type Tabler interface {
	TableName() string
}

func (PokemonModel) TableName() string {
	return "pokemon"
}

func (TypeModel) TableName() string {
	return "type"
}
