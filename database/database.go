package database

import (
	"sync"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type Database struct {
	*gorm.DB
	mu sync.Mutex
}

func InitDB() (*Database, error) {
	db, err := gorm.Open(sqlite.Open("pokemon.sqlite"), &gorm.Config{})
	if err != nil {
		return nil, err
	}

	if err = db.AutoMigrate(&PokemonModel{}, &TypeModel{}); err != nil {
		return nil, err
	}

	return &Database{DB: db}, nil
}

func (db *Database) CreateRecord(model interface{}) error {
	db.mu.Lock()
	defer db.mu.Unlock()

	result := db.Create(model)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (db *Database) GetTableLength(tableName string) (int64, error) {
	var count int64
	result := db.Table(tableName).Count(&count)
	if result.Error != nil {
		return 0, result.Error
	}
	return count, nil
}

func (db *Database) GetAllTypeModels() ([]*TypeModel, error) {
	var types []*TypeModel
	result := db.Find(&types)
	if result.Error != nil {
		return nil, result.Error
	}
	return types, nil
}

func (db *Database) GetAllPokemonModels() ([]*PokemonModel, error) {
	var pokemon []*PokemonModel
	result := db.Find(&pokemon)
	if result.Error != nil {
		return nil, result.Error
	}
	return pokemon, nil
}
