module pokemonScraper

go 1.21.6

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/gdamore/tcell/v2 v2.6.1-0.20231203215052-2917c3801e73
	github.com/gocolly/colly v1.2.0
	github.com/rivo/tview v0.0.0-20240204151237-861aa94d61c8
	gorm.io/driver/sqlite v1.5.5
	gorm.io/gorm v1.25.7-0.20240204074919-46816ad31dde
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/antchfx/htmlquery v1.3.0 // indirect
	github.com/antchfx/xmlquery v1.3.18 // indirect
	github.com/antchfx/xpath v1.2.4 // indirect
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/gobwas/glob v0.2.3 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/kennygrant/sanitize v1.2.4 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	github.com/rivo/uniseg v0.4.7-0.20240127222946-601bbb3750c2 // indirect
	github.com/saintfish/chardet v0.0.0-20230101081208-5e3ef4b5456d // indirect
	github.com/temoto/robotstxt v1.1.2 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/term v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
