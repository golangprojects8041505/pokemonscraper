package gui

import (
	"fmt"
	"github.com/rivo/tview"
	"github.com/gdamore/tcell/v2"
	"log"
	"pokemonScraper/components"
	"sort"
	"strings"
)

var app = tview.NewApplication()
var pages = tview.NewPages()
var mainOptions = []string{"Show All Pokemon", "Show Pokemon Types", "Find Pokemon", "Quit (press q)"}
var mainList = tview.NewList()
var subList = tview.NewList()
var findFlex = tview.NewFlex()
var pokemonInfo = tview.NewTextView()
var pokemonPage = tview.NewFlex()

func RunApp(allTypes components.AllTypes, allPokemons components.AllPokemons) {
	mainList.AddItem(mainOptions[0], "", '1', func() {
		showAllPokemon(allPokemons)
	})
	mainList.AddItem(mainOptions[1], "", '2', func() {
		showTypes(allTypes)
	})
	mainList.AddItem(mainOptions[2], "", '3', func() {
		findPokemon(allPokemons)
	})
	mainList.AddItem(mainOptions[3], "", 'q', func() {
		app.Stop()
	})

	pages.AddPage("Menu", mainList, true, true)
	pages.AddPage("All Pokemons", subList, true, false)
	pages.AddPage("All Types", subList, true, false)
	pages.AddPage("Find Pokemon", findFlex, true, false)
	pages.AddPage("Pokemon Page", pokemonPage, true, false)

	if err := app.SetRoot(pages, true).Run(); err != nil {
		log.Panic(err)
	}
}
func showAllPokemon(allPokemon components.AllPokemons) {
	pages.SwitchToPage("All Pokemons")
	subList.Clear()
	subList.AddItem("Go to Menu (press q)", "", 'q', func() {
		pages.SwitchToPage("Menu")
	})
	var pokemonSlice []*components.Pokemon
	for _, pokemon := range allPokemon {
		pokemonSlice = append(pokemonSlice, pokemon)
	}
	sort.Slice(pokemonSlice, func(i, j int) bool {
		return pokemonSlice[i].ID < pokemonSlice[j].ID
	})

	for _, pokemon := range pokemonSlice {
		subList.AddItem(fmt.Sprintf("%s - %s", pokemon.ID, pokemon.Name), "", 0, nil)
	}
}

func showTypes(allTypes components.AllTypes) {
	pages.SwitchToPage("All Types")
	subList.Clear()
	subList.AddItem("Go to Menu (press q)", "", 'q', func() {
		pages.SwitchToPage("Menu")
	})

	for _, pokemonType := range allTypes {
		subList.AddItem("--------------------------------------------------", "", 0, nil)
		subList.AddItem(fmt.Sprintf("Type: %s", pokemonType.Name), "", 0, nil)
		strengths := "Strengths: " + concatenateTypeNames(pokemonType.Strengths)
		weaknesses := "Weakenesses: " + concatenateTypeNames(pokemonType.Weaknesses)
		subList.AddItem(strengths, "", 0, nil)
		subList.AddItem(weaknesses, "", 0, nil)
	}
}

func findPokemon(allPokemons components.AllPokemons) {
	findFlex.Clear()
	var inputField *tview.InputField
	inputField = tview.NewInputField().
	SetLabel("Enter Pokemon Name:").
	SetFieldWidth(30).
	SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEnter {
			pokemonName := inputField.GetText()
			inputField.SetText("")
			showSinglePokemon(allPokemons, pokemonName, findFlex, false)
		}
	})
	findFlex.AddItem(inputField, 0, 1, true)
	pages.SwitchToPage("Find Pokemon")
}

func concatenateTypeNames(names []*components.Type) string { 
	var result string
	for i, element := range names {
		if i > 0 {
			result += ", "
		}
		result += element.Name
	}
	return result
}

func showSinglePokemon(allPokemons components.AllPokemons, pokemonName string, flex *tview.Flex, clear bool) {
	if !clear {
		if pokemonName == "q" {
			pages.SwitchToPage("Menu")
		}
	} else {
		pages.SwitchToPage("Pokemon Page")
	}
	flex.RemoveItem(pokemonInfo)
	pokemon, ok := allPokemons[strings.Title(pokemonName)]
	var text string
	if !ok {
		text = "There is no such Pokemon"
	} else {
		text = fmt.Sprintf(
			"Name: %s\n"+
			"Description: %s\n"+
			"ID: %s\n"+
			"Species: %s\n"+
			"Types: %v\n"+
			"Height: %s\n"+
			"Weight: %s\n"+
			"Evolutions: %v",
			pokemon.Name, pokemon.Description, pokemon.ID, pokemon.Species,
			pokemon.Types, pokemon.Height, pokemon.Weight, pokemon.Evolutions,
		)
	}
	pokemonInfo.SetText(text)
	flex.AddItem(pokemonInfo, 0, 1, true)
}