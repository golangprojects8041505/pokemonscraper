package setup

import (
	"log"
	"pokemonScraper/components"
	"pokemonScraper/database"
	"pokemonScraper/scraper"
	"sync"
)

func SetUpData() (components.AllTypes, components.AllPokemons, error) {
	var allTypes components.AllTypes
	var allPokemon components.AllPokemons

	db, err := database.InitDB()
	if err != nil {
		return nil, nil, err
	}
	typesCount, err := db.GetTableLength("type")
	if err != nil {
		return nil, nil, err
	}
	pokemonCount, err := db.GetTableLength("pokemon")
	if err != nil {
		return nil, nil, err
	}
	typesInDatabase := typesCount == 18
	pokemonInDatabase := pokemonCount == 985

	if typesInDatabase {
		log.Println("Getting types from database")
		typeModels, err := db.GetAllTypeModels()
		if err != nil {
			return nil, nil, err
		}
		allTypes = components.ConvertToAllTypes(typeModels)
	} else {
		log.Println("Scraping data about types")
		allTypes, err = scraper.GetCollector().CreateTypes()
		if err != nil {
			return nil, nil, err
		}
	}

	if pokemonInDatabase {
		log.Println("Getting pokemon from database")
		pokemonModels, err := db.GetAllPokemonModels()
		if err != nil {
			return nil, nil, err
		}
		allPokemon = components.ConvertToAllPokemons(pokemonModels, allTypes)
	} else {
		log.Println("Scraping data about pokemon")
		allPokemon, err = scraper.GetCollector().CreatePokemon(allTypes)
		if err != nil {
			return nil, nil, err
		}
	}

	if !typesInDatabase {
		var wg sync.WaitGroup
		for _, pokemonType := range allTypes {
			wg.Add(1)
			go func(pokemonType *components.Type, wg *sync.WaitGroup) {
				defer wg.Done()
				model := components.ConvertTypeToModel(pokemonType)
				err := db.CreateRecord(model)
				if err != nil {
					log.Fatal(err)
				}
			}(pokemonType, &wg)
		}
		wg.Wait()
	}
	if !pokemonInDatabase {
		var wg sync.WaitGroup
		for _, pokemon := range allPokemon {
			wg.Add(1)
			go func(pokemon *components.Pokemon, wg *sync.WaitGroup) {
				defer wg.Done()
				model := components.ConvertPokemonToModel(pokemon)
				err := db.CreateRecord(model)
				if err != nil {
					log.Fatal(err)
				}
			}(pokemon, &wg)
		}
		wg.Wait()
	}
	return allTypes, allPokemon, nil
}
