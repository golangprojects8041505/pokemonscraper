package main

import (
	"log"
	"pokemonScraper/gui"
	"pokemonScraper/setup"
)

func main() {
	allTypes, allPokemon, err := setup.SetUpData()
	if err != nil {
		log.Panic(err)
	}
	gui.RunApp(allTypes, allPokemon)
}
