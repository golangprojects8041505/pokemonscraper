package components

type Pokemon struct {
	Name        string
	Description string
	ID          string
	Species     string
	Types       []*Type
	Height      string
	Weight      string
	Evolutions  []string
}

type Type struct {
	Name       string
	Strengths  []*Type
	Weaknesses []*Type
}

type AllTypes map[string]*Type
type AllPokemons map[string]*Pokemon
