package components

import (
	"pokemonScraper/database"
	"strings"
)

func ConvertPokemonToModel(pokemon *Pokemon) *database.PokemonModel {
	var types []string
	for _, pokemonType := range pokemon.Types {
		types = append(types, pokemonType.Name)
	}
	return &database.PokemonModel{
		Name:        pokemon.Name,
		Description: pokemon.Description,
		PokedexID:   pokemon.ID,
		Species:     pokemon.Species,
		Types:       strings.Join(types, ","),
		Height:      pokemon.Height,
		Weight:      pokemon.Weight,
		Evolutions:  strings.Join(pokemon.Evolutions, ","),
	}
}

func ConvertTypeToModel(pokemonType *Type) *database.TypeModel {
	var strengths []string
	for _, strength := range pokemonType.Strengths {
		strengths = append(strengths, strength.Name)
	}

	var weaknesses []string
	for _, weakness := range pokemonType.Weaknesses {
		weaknesses = append(weaknesses, weakness.Name)
	}
	return &database.TypeModel{
		Name:       pokemonType.Name,
		Strengths:  strings.Join(strengths, ","),
		Weaknesses: strings.Join(weaknesses, ","),
	}
}

func ConvertToAllTypes(typeModels []*database.TypeModel) AllTypes {
	allTypes := make(AllTypes)
	for _, typeModel := range typeModels {
		allTypes[typeModel.Name] = &Type{
			Name: typeModel.Name,
		}
	}
	for _, typeModel := range typeModels {
		strengths := strings.Split(typeModel.Strengths, ",")
		for _, strength := range strengths {
			if addedStrength := allTypes[strength]; addedStrength != nil {
				allTypes[typeModel.Name].Strengths = append(allTypes[typeModel.Name].Strengths, allTypes[strength])
			}
		}
		weaknesses := strings.Split(typeModel.Weaknesses, ",")
		for _, weakness := range weaknesses {
			if addedWeakness := allTypes[weakness]; addedWeakness != nil {
				allTypes[typeModel.Name].Weaknesses = append(allTypes[typeModel.Name].Weaknesses, allTypes[weakness])
			}
		}
	}
	return allTypes
}

func ConvertToAllPokemons(pokemonModels []*database.PokemonModel, allTypes AllTypes) AllPokemons {
	allPokemons := make(AllPokemons)
	for _, pokemonModel := range pokemonModels {
		typesName := strings.Split(pokemonModel.Types, ",")
		var pokemonTypes []*Type
		for _, typeName := range typesName {
			pokemonTypes = append(pokemonTypes, allTypes[typeName])
		}
		evolutions := strings.Split(pokemonModel.Evolutions, ",")
		allPokemons[pokemonModel.Name] = &Pokemon{
			Name:        pokemonModel.Name,
			Description: pokemonModel.Description,
			ID:          pokemonModel.PokedexID,
			Species:     pokemonModel.Species,
			Types:       pokemonTypes,
			Height:      pokemonModel.Height,
			Weight:      pokemonModel.Weight,
			Evolutions:  evolutions,
		}
	}
	return allPokemons
}
