package scraper

import (
	"fmt"
	"pokemonScraper/components"
	"slices"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
)

type PokemonCollector struct {
	*colly.Collector
}

func GetCollector() *PokemonCollector {
	c := colly.NewCollector()
	c.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
	return &PokemonCollector{c}
}

func (pc *PokemonCollector) CreateTypes() (components.AllTypes, error) {
	pokemonTypes := make(components.AllTypes)
	typesName, err := pc.getAllTypes()
	if err != nil {
		return nil, err
	}
	for _, typeName := range typesName {
		newType := components.Type{Name: typeName}
		pokemonTypes[typeName] = &newType
	}
	var wg sync.WaitGroup
	for _, typeStruct := range pokemonTypes {
		wg.Add(1)
		go GetCollector().getTypeInfo(typeStruct, pokemonTypes, &wg)
	}
	wg.Wait()
	return pokemonTypes, nil
}

func (pc *PokemonCollector) CreatePokemon(pokemonTypes components.AllTypes) (components.AllPokemons, error) {
	allPokemons, err := pc.getAllPokemon()
	if err != nil {
		return nil, err
	}
	var wg sync.WaitGroup
	for _, pokemon := range allPokemons {
		wg.Add(1)
		go GetCollector().getPokemonInfo(pokemon, pokemonTypes, &wg)
	}
	wg.Wait()
	for _, pokemon := range allPokemons {
		wg.Add(1)
		go GetCollector().addEvolutions(pokemon, allPokemons, &wg)
	}
	wg.Wait()
	return allPokemons, nil
}

func (pc *PokemonCollector) getAllPokemon() (components.AllPokemons, error) {
	allPokemons := make(components.AllPokemons)
	var err error
	pc.OnHTML("td.cell-name", func(e *colly.HTMLElement) {
		pokemon := components.Pokemon{}
		pokemon.Name = e.ChildText("a")
		secondName := e.ChildText("small")
		if len(secondName) == 0 {
			allPokemons[pokemon.Name] = &pokemon
		}
	})
	pc.OnError(func(r *colly.Response, e error) {
		err = e
	})
	pc.Visit("https://pokemondb.net/pokedex/all")
	if err != nil {
		return nil, err
	}
	if len(allPokemons) != 985 {
		return nil, fmt.Errorf("not all Pokemon have been found")
	}
	return allPokemons, nil
}

func (pc *PokemonCollector) getPokemonInfo(pokemon *components.Pokemon, pokemonTypes components.AllTypes, wg *sync.WaitGroup) error {
	defer wg.Done()
	var err error

	pc.OnHTML("div.sv-tabs-panel.active > div > div:has(h2:contains('Pokédex data')) > table > tbody", func(e *colly.HTMLElement) {
		pokemon.ID = e.ChildText("strong")

		e.DOM.Find("tr:nth-child(2) td").Each(func(_ int, s *goquery.Selection) {
			typesString := strings.TrimSpace(s.Text())
			types := strings.Split(typesString, " ")
			for _, pokemonType := range types {
				pokemon.Types = append(pokemon.Types, pokemonTypes[pokemonType])
			}
		})
		e.DOM.Find("tr:nth-child(3) td").Each(func(_ int, s *goquery.Selection) {
			pokemon.Species = s.Text()
		})
		e.DOM.Find("tr:nth-child(4) td").Each(func(_ int, s *goquery.Selection) {
			pokemon.Height = s.Text()
		})
		e.DOM.Find("tr:nth-child(5) td").Each(func(_ int, s *goquery.Selection) {
			pokemon.Weight = s.Text()
		})
	})
	pc.OnHTML("#main > p:nth-child(n):nth-child(-n+6)", func(e *colly.HTMLElement) {
		pokemon.Description = strings.TrimSpace(fmt.Sprintf("%s\n%s", pokemon.Description, e.Text))
	})
	pc.OnError(func(r *colly.Response, e error) {
		err = e
	})
	if err != nil {
		return err
	}
	urlName := replaceForUrl(pokemon.Name)
	url := fmt.Sprintf("https://pokemondb.net/pokedex/%s", urlName)
	pc.Visit(url)
	return nil
}

func (pc *PokemonCollector) getAllTypes() ([]string, error) {
	var TypesName []string
	var err error

	pc.OnHTML("p.text-center", func(e *colly.HTMLElement) {
		e.ForEach("a", func(_ int, a *colly.HTMLElement) {
			typeName := a.Text
			TypesName = append(TypesName, typeName)
		})
	})
	pc.OnError(func(r *colly.Response, e error) {
		err = e
	})
	pc.Visit("https://pokemondb.net/type")

	if err != nil {
		return nil, err
	}
	if len(TypesName) == 0 {
		return nil, fmt.Errorf("error getting all types")
	}
	return TypesName, nil
}

func (pc *PokemonCollector) getTypeInfo(typeStruct *components.Type, pokemonTypes components.AllTypes, wg *sync.WaitGroup) {
	defer wg.Done()
	selector := fmt.Sprintf("td[title^='%s']", typeStruct.Name)
	pc.OnHTML(selector, func(e *colly.HTMLElement) {
		effectiveness := strings.Split(e.Attr("title"), " = ")
		if effectiveness[1] != "normal effectiveness" {
			effectiveType := strings.Split(effectiveness[0], " → ")[1]
			switch effectiveness[1] {
			case "not very effective", "no effect":
				typeStruct.Weaknesses = append(typeStruct.Weaknesses, pokemonTypes[effectiveType])
			case "super-effective":
				typeStruct.Strengths = append(typeStruct.Strengths, pokemonTypes[effectiveType])
			}
		}
	})
	pc.Visit("https://pokemondb.net/type")
}

func (pc *PokemonCollector) addEvolutions(pokemon *components.Pokemon, allPokemons components.AllPokemons, wg *sync.WaitGroup) {
	defer wg.Done()
	pc.OnHTML("div.infocard-list-evo", func(e *colly.HTMLElement) {
		evolutions := e.ChildAttrs("img", "alt")
		slices.Sort(evolutions)
		evolutions = slices.Compact(evolutions)
		for _, evolution := range evolutions {
			if evolution != pokemon.Name {
				pokemon.Evolutions = append(pokemon.Evolutions, evolution)
			}
		}
	})
	url := fmt.Sprintf("https://pokemondb.net/pokedex/%s", pokemon.Name)
	pc.Visit(url)
}

func replaceForUrl(name string) string {
	replacements := map[string]string{
		": ": "-",
		". ": "-",
		" ":  "-",
		"'":  "",
		"♀":  "-f",
		"♂":  "-m",
		".":  "",
		"é":  "e",
	}

	urlName := name
	for old, new := range replacements {
		urlName = strings.ReplaceAll(urlName, old, new)
	}

	return urlName
}
